import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  constructor(
    //1.Membuat Dialog
    public dialogRef:MatDialogRef<ProductDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public api:ApiService
  ) { }

  ngOnInit(): void {
  }

  //Membuat fungsi loading:
  loading!: boolean;
  
  //Fungsi Save Data:
  saveData()
  {
    this.loading=true;

    //jika id tidak terdefinisi maka buat data
    if(this.data.id ==undefined)
    {
      //prosedur pengiriman data ke server menggunakan metode POST
      this.api.post('bookswithauth',this.data).subscribe(result=>{
        //tutup dialog dan kirimkan feedback server ke fungsi pemanggil dialog
        this.dialogRef.close(result);
        this.loading=false;
      },error=>{
        this.loading=false;
        alert('Tidak Dapat Menyimpan Data. Silahkan Coba Lagi!');
      });

    } else{
      //prosedur edit data menggunakan metode PUT  
      this.api.put('bookswithauth/'+this.data.id,this.data).subscribe(result=>{
        //tutup dialog dan kirimkan feedback server ke fungsi pemanggil dialog
        this.dialogRef.close(result);
        console.log(result);
      },error=>{
        this.loading=false;
        alert('Tidak Dapat Memperbaharui Data. Silahkan Coba Lagi!');
      })
    }


  }
}
