import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-files-uploader',
  templateUrl: './files-uploader.component.html',
  styleUrls: ['./files-uploader.component.scss']
})
export class FilesUploaderComponent implements OnInit {

  constructor(
    public api: ApiService,
    public dialogRef:MatDialogRef<FilesUploaderComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData:any
  ) { }

  ngOnInit(): void {
    console.log(this.dialogData);
  }

  //Membuat Fungsi Seleksi File
  selectedFile:any;   
  onFileChange(event:any) {
   if(event.target.files.length > 0) {
       this.selectedFile=event.target.files[0];
    if (this.selectedFile.type != 'application/pdf')
        alert('Tipe file harus PDF.');
        console.log(this.selectedFile);        
   }
}

  //Membuat Fungsi Upload File
  loadingUpload!: boolean;
  uploadFile() {
   let input = new FormData();
   input.append('file', this.selectedFile);
   this.loadingUpload = true;  
   this.api.upload(input).subscribe(data=>{ 
     //lakukan update data produk disini
     this.updateProduct(data);   
     //
     console.log(data);
   },error=>{
       this.loadingUpload = false;
       alert('Gagal mengunggah file');
   });
 }

 //Membuat Fungsi Update Product
 updateProduct(data:any)
 {
   if(data.status == true)
   {
     //lakukan update data produk disini
     this.updateBook(data);
     //

     alert('File berhasil diunggah');
     this.loadingUpload = false;
     this.dialogRef.close();
   }else {
     alert(data.message);
     
   }
 }

 //Membuat Fungsi Update Buku
 updateBook(data:any)
{
  this.api.put('books/'+this.dialogData.id, {url: data.url}).subscribe(res=>{
    console.log(res);
  })
}


}
