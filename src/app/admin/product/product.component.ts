import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import * as FileSaver from 'file-saver';
import { ApiService } from 'src/app/services/api.service';
import { FilesUploaderComponent } from '../files-uploader/files-uploader.component';
import { ProductDetailComponent } from '../product-detail/product-detail.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  //1.Membuat Title:
  title:any;

  //2.Membuat Koleksi Book:
  book:any={};
  
  //3.Membuat Koleksi Books:
  books:any=[];
  constructor(
    public dialog:MatDialog,
    public api:ApiService
  ) 
  { 
   
  }

  ngOnInit(): void {
    
    //4.Memanggil fungsi title:
    this.title='Products';

    //5.Memanggil fungsi book: 
    this.book={
        title : 'The First Experience How To Use Angular For Beginner',
        author : 'Berliana Andzini Perdana',
        publisher: 'Ahmad Dahlan University',
        year : 2021,
        isbn :'9090909090',
        price: 90000
    };
    this.getBooks();
  }

  //Membuat fungsi loading:
  loading!: boolean;

  //6.Memanggil fungsi books:
getBooks()
{
  this.loading=true;
  this.api.get('bookswithauth').subscribe(result=>{
    this.books=result;
    this.loading=false;
  },error=>
    {
    this.loading=false;
    alert('Ada Masalah Saat Pengambilan Data. Silahkan Coba Lagi!');
    })


  /*
  this.loading=true;
  this.api.get('books').subscribe(result=>{
    this.books=result;
    this.loading=false;
  },error=>
    {
    this.loading=false;
    alert('Tidak Dapat Memperbaharui Data. Silahkan Coba Lagi!');
    })
  */
}

//7.Membuat fungsi product detail:
productDetail(data: any,idx: number){
  let dialog=this.dialog.open(ProductDetailComponent, {
    width:'400px',
    data:data
  } );

  dialog.afterClosed().subscribe(res=>{
    if(res)
    {
      //jika idx=-1 (penambahan data baru) maka tambahkan data
      if(idx==-1)this.books.push(res);
      //jika tidak maka perbarui data
      else this.books[idx]=data;
      }
  })

}

//Membuat fungsi UploadFile:
upload(data: any,idx: any){
  let dialog=this.dialog.open(FilesUploaderComponent, {
    width:'400px',
    data:data
  } );

  dialog.afterClosed().subscribe(res=>{
    return;
  })
}


//Membuat fungsi loading:
loadingDelete:any={};

//Membuat fungsi delete:
deleteProduct(id: any,idx: string | number)
  {
    var conf=confirm('Are You Sure Want To Delete This Item?');
    if(conf) 
    {
      this.loadingDelete[idx]=true;
      this.api.delete('bookswithauth/'+this.books[idx].id).subscribe(result=>{
        this.books.splice(idx,1);
        this.loadingDelete[idx]=false;
      },error=>{
        this.loadingDelete[idx]=false;
        alert('Tidak Dapat Menghapus Data. Silahkan Coba Lagi!');
      });   
    }
  }

  //Membuat fungsi download file:
  downloadFile(data:any)
  {
    FileSaver.saveAs('http://api.sunhouse.co.id/bookstore/'+data.url);
  }

}
